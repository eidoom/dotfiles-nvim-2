# [dotfiles-nvim-2](https://gitlab.com/eidoom/dotfiles-nvim-2)

[Companion post](https://computing-blog.netlify.app/post/nvim2/)

Install
```shell
sudo dnf install neovim
git clone git@gitlab.com:eidoom/dotfiles-nvim-2.git ~/git/dotfiles-nvim-2
ln -s ~/git/dotfiles-nvim-2 ~/.config/nvim
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
nvim
:PlugInstall
```

## Formatters

```shell
sudo npm i -g prettier prettier-plugin-svelte
sudo dnf install -y python3 python3-pip
python3 -m pip install --upgrade --user neovim pynvim black
```

## License
dotfiles-nvim-2 (c) by Ryan Moodie

dotfiles-nvim-2 is licensed under a
Creative Commons Attribution 4.0 International License.

You should have received a copy of the license along with this
work under <./LICENSE>.  If not, see 
* summary: <http://creativecommons.org/licenses/by/4.0/>.
* license: <https://creativecommons.org/licenses/by/4.0/legalcode>.
* plain text: <https://creativecommons.org/licenses/by/4.0/legalcode.txt>.
