let b:formatdef_custom_svelte='"prettier --write --parser=svelte"'
let b:formatters_svelte = ['custom_svelte']

setlocal commentstring=<!--\ %s\ -->
