" Go template files

setlocal commentstring={{/*\ %s\ */}}

let b:formatdef_custom_template='"prettier --write --parser=html"'
let b:formatters_template = ['custom_template']
