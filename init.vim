" plugin polyglot configuration
" I don't like how csv.vim displays the file
" vim-mma breaks bracket pair highlighting
" I don't use Visual Basic and it interferes with FORM *.frm (made pull
" request with fix <https://github.com/sheerun/vim-polyglot/pull/824>)
let g:polyglot_disabled = [
			\ 'csv',
			\ 'mathematica',
			\ 'visual-basic',
			\ ]

" plugins
call plug#begin('~/.config/nvim/plugged')
" turn default register into a stack
Plug 'maxbrunsfeld/vim-yankstack'
" comments
Plug 'tpope/vim-commentary'
" show buffers at bottom
Plug 'bling/vim-bufferline'
" language syntax support
Plug 'sheerun/vim-polyglot'
" autoformat
Plug 'Chiel92/vim-autoformat'
" solarised colour scheme supporting 24-bit colour
Plug 'overcache/NeoSolarized'
" Janet syntax files
Plug 'bakpakin/janet.vim', { 'for': 'janet' }
" Futhark syntax files
Plug 'BeneCollyridam/futhark-vim', { 'for': 'fut' }
call plug#end()

" mousemode
set mouse=a

" navigation
map h <Nop>
noremap ; <Right>
noremap l <Up>
noremap k <Down>
noremap j <Left>

" use the system clipboard as the default register
set clipboard=unnamedplus

" butterfingers
cnoreabbrev Noh noh
cnoreabbrev Q q
cnoreabbrev Q! q!
cnoreabbrev Qa qa
cnoreabbrev Qa! qa!
cnoreabbrev W w
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev Bn bn
cnoreabbrev Bp bp

" for polyglot
set nocompatible

" leaders
let mapleader=' '
nnoremap <leader>a :Autoformat<CR>
nnoremap <leader>d :bp<CR>
nnoremap <leader>f :bn<CR>

" enable 24-bit colour
set termguicolors

" set colour scheme
if $COLOUR_SCHEME == "dark"
	set background=dark
else
	set background=light
endif

colorscheme NeoSolarized

" instead of st
au BufRead,BufNewFile *.cls set filetype=tex

" vim-polyglot mma disabled
au BufRead,BufNewFile *.wl,*.m set filetype=mma

" something is overwriting the after/ftplugin/mma.vim commentstring
" this is a temporary fix
au BufRead,BufNewFile *.wl,*.m setlocal commentstring=(*\ %s\ *)

" automatic file type detection: loads settings from ftplugin/
filetype plugin on
